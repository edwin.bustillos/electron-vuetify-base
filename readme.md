## Eletron Project Vuetify Base
```
npm install -g @vue/cli
vue create . or electron-app-name
vue add vuetify
vue add electron-builder
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run electron:serve
```

### Compiles and minifies for production
```
npm run electron:build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
